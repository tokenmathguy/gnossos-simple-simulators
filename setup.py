import os
from setuptools import setup


def read(fname):
        return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="gnossos_simple_simulators",
    version="0.0.1",
    author="Eric Johnson",
    author_email="tokenmathguy@gmail.com",
    description=("Simple game simulators that can play variations of Gnossos"),
    license="BSD",
    keywords="board games AI artificial intelligence simulators gnossos",
    url="https://bitbucket.org/tokenmathguy/gnossos-simple-simulators",
    packages=['gnossos_simple_simulators', 'tests'],
    long_description=read('README'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "License :: OSI Approved :: BSD License",
    ],
)
