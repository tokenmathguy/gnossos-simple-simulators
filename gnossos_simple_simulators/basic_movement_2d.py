#!/usr/bin/env python
from __future__ import print_function

import sys
import argparse
import pdb

from collections import namedtuple
from easyAI import (
    TwoPlayersGame,
    AI_Player,
    Negamax,
    id_solve
)
from easyAI.AI.TT import TT

COLOR_MAP = {
    'black': '\033[0;30m',
    'red': '\033[0;31m',
    'green': '\033[0;32m',
    'brown': '\033[0;33m',
    'blue': '\033[0;34m',
    'purple': '\033[0;35m',
    'cyan': '\033[0;36m',
    'light_gray': '\033[0;37m',
    'dark_gray': '\033[1;30m',
    'light_red': '\033[1;31m',
    'light_green': '\033[1;32m',
    'yellow': '\033[1;33m',
    'light_blue': '\033[1;34m',
    'light_purple': '\033[1;35m',
    'light_cyan': '\033[1;36m',
    'white': '\033[1;37m',
    'endc': '\033[0m'}
Colors = namedtuple('Colors', COLOR_MAP.keys())
COLORS = Colors(**COLOR_MAP)

Position = namedtuple('Position', ['x', 'y'])


def distance(position1, position2):
    return abs(position1.x - position2.x) + abs(position1.y - position2.y)


def magnitude(move):
    return abs(move.x) + abs(move.y)


def ball(center, radius):
    return [Position(x=center.x + dx, y=center.y + dy)
            for dx in range(center.x - radius, center.x + radius + 1)
            for dy in range(center.y - radius, center.y + radius + 1)
            if dx + dy <= radius]


class VillagerAI(AI_Player):
    def __init__(self, ai, position, tokens=30, units_per_hostage=25):
        AI_Player.__init__(self, ai)
        self.name = "{}villager{}".format(COLORS.light_blue, COLORS.endc)
        self.position = position
        self.max_move = 3
        self.all_moves = ball(Position(x=0, y=0), self.max_move)

        self.tokens = tokens
        self.units_per_hostage = units_per_hostage
        self.units = 0

    def move_cost(self, move):
        abs_move = magnitude(move)
        if abs_move <= 4:
            return 1
        elif abs_move <= 7:
            return 2
        else:
            return 3

    def tally_move(self, move, sign=1):
        self.position = Position(x=self.position.x + sign * move.x, y=self.position.y + sign * move.y)
        self.tokens -= sign * self.move_cost(move)
        self.units += sign * magnitude(move)

    def make_move(self, move):
        self.tally_move(move)

    def unmake_move(self, move):
        self.tally_move(move, -1)

    def win(self, minotaur):
        return self.hostages >= 4 and self.tokens > 0 and self.position != minotaur.position

    @property
    def game_key(self):
        return tuple([self.position, self.tokens, self.units])

    @property
    def hostages(self):
        return self.units / self.units_per_hostage

    def __str__(self):
        return "{name} position: {position}, tokens: {tokens}, hostages: {hostages} ".format(
            name=self.name,
            position=self.position,
            tokens=self.tokens,
            hostages=self.hostages)


class MinotaurAI(AI_Player):
    def __init__(self, ai, position):
        AI_Player.__init__(self, ai)
        self.name = "{}minotaur{}".format(COLORS.light_purple, COLORS.endc)
        self.position = position
        self.max_move = 2
        self.all_moves = ball(Position(x=0, y=0), self.max_move)

    def tally_move(self, move, sign=1):
        self.position = Position(x=self.position.x + sign * move.x, y=self.position.y + sign * move.y)

    def make_move(self, move):
        self.tally_move(move)

    def unmake_move(self, move):
        self.tally_move(move, -1)

    def win(self, villager):
        return self.position == villager.position or villager.tokens <= 0

    @property
    def game_key(self):
        return tuple([self.position])

    def __str__(self):
        return "{name} position: {position}".format(name=self.name, position=self.position)


class GnossosBasicMovement2D(TwoPlayersGame):
    """In turn, the players move some units.

    The villager player wins if it can collect 4 hostages.

    The minotaur player wins if it can occupy the same position or if the villager runs out of tokens"""
    def __init__(self, villager, minotaur, board_width, board_length, verbose=False):
        self.villager = villager
        self.minotaur = minotaur
        self.players = [villager, minotaur]
        self.nplayer = 1

        # defaults
        self.board_width = board_width
        self.board_length = board_length
        self.board_diameter = board_width + board_length
        self.initial_tokens = villager.tokens
        self.verbose = verbose

    def possible_moves(self):
        possible = []
        for move in self.player.all_moves:
            newx, newy = self.player.position.x + move.x, self.player.position.y + move.y
            if newx >= 0 and newx < self.board_width and newy >= 0 and newy < self.board_length:
                possible.append(move)

        return possible

    def make_move(self, move):
        self.player.make_move(move)

    def unmake_move(self, move):
        self.player.unmake_move(move)

    def declare_outcome(self, perspective=1):
        if self.nplayer == perspective:
            return self.villager.win(self.minotaur)
        else:
            return self.minotaur.win(self.villager)

    def win(self):
        return self.declare_outcome(perspective=1)

    def lose(self):
        return self.declare_outcome(perspective=2)

    def is_over(self):
        return self.win() or self.lose()

    def scoring(self):
        """
        May want to plug in a heuristic for non-wins such as

            villager_points = int(self.villager.hostages * 25)
            minotaur_distance_heuristic = ((
                self.board_diameter - abs(6 - distance(self.villager.position, self.minotaur.position))) /
                self.board_diameter)
            minotaur_token_heuristic = abs(self.initial_tokens - self.villager.tokens) / self.initial_tokens
            minotaur_points = int(49 * (minotaur_distance_heuristic + minotaur_token_heuristic))
            val = villager_points - minotaur_points

        also, if you want to show all scores in the presence of this heuristic, make sure val != 0 is removed as a
        condition
        """
        if self.villager.win(self.minotaur):
            val = 100
        elif self.minotaur.win(self.villager):
            val = -100
        else:
            val = 0

        if val != 0 and self.verbose:
            print("{} ".format(val), end="")
            self.show()
        return val

    def show(self):
        """Just for prettifying the output of a played game. Not necessary
        """
        print("{green}{win}{endc}{red}{lose}{endc} {player} {minotaur} {villager}".format(
            green=COLORS.green,
            red=COLORS.red,
            endc=COLORS.endc,
            win="WIN" if self.win() else "",
            lose="LOSE" if self.lose() else "",
            player=self.player.name,
            minotaur=self.minotaur,
            villager=self.villager))


def main(argv):  # pragma: no cover
    """Sample execution for solving

    python build/lib.linux-x86_64-2.7/gnossos_simple_simulators/basic_movement_2d.py \
        --tokens 40 --board-width 30 --board-length 40 \
        --villager-pos-x 10 --villager-pos-y 10 --minotaur-pos-x 20 --minotaur-pos-y 20 \
        --units-per-hostage 21 --verbose \
        --solve --solver-depth-min 2 --solver-depth-max 18
    """
    parser = argparse.ArgumentParser("Executes the specified game")
    parser.add_argument("-a", "--moves-ahead", dest="moves_ahead", default=8,
                        help="number of moves ahead each player should look")
    parser.add_argument("-w", "--board-width", dest="board_width", default=30,
                        help="the initial width of the board")
    parser.add_argument("-l", "--board-length", dest="board_length", default=30,
                        help="the initial length of the board")

    parser.add_argument("-vx", "--villager-pos-x", dest="villager_pos_x", default=10,
                        help="start x-position of the villager")
    parser.add_argument("-vy", "--villager-pos-y", dest="villager_pos_y", default=10,
                        help="start y-position of the villager")
    parser.add_argument("-mx", "--minotaur-pos-x", dest="minotaur_pos_x", default=20,
                        help="start position-x of the minotaur")
    parser.add_argument("-my", "--minotaur-pos-y", dest="minotaur_pos_y", default=20,
                        help="start position-y of the minotaur")

    parser.add_argument("-t", "--tokens", dest="tokens", default=30,
                        help="number of tokens the villager starts with")
    parser.add_argument("-u", "--units-per-hostage", dest="units_per_hostage", default=20,
                        help="units to move per hostage collected")

    parser.add_argument("--solve", dest="solve", action="store_true",
                        help="indicates whether to solve the game, rather than playing a round")
    parser.add_argument("--solver-depth-min", dest="solver_depth_min", default=2,
                        help="depth that the AIs are allowed to iterate from")
    parser.add_argument("--solver-depth-max", dest="solver_depth_max", default=10,
                        help="depth that the AIs are allowed to iterate to")
    parser.add_argument("--win-score", dest="win_score", default=99,
                        help="win score to mark game as solved")

    parser.add_argument("--pdb", dest="do_pdb", action="store_true",
                        help="break into pdb prior to playing the game or solving")

    parser.add_argument("--verbose", dest="verbose", action="store_true",
                        help="indicates whether to print more info during calculation")
    args = parser.parse_args()

    villager = VillagerAI(
        ai=Negamax(args.moves_ahead),
        position=Position(x=int(args.villager_pos_x), y=int(args.villager_pos_y)),
        tokens=int(args.tokens),
        units_per_hostage=float(args.units_per_hostage))
    minotaur = MinotaurAI(
        ai=Negamax(int(args.moves_ahead)),
        position=Position(x=int(args.minotaur_pos_x), y=int(args.minotaur_pos_y)))

    def tt_key(game):
        return tuple([game.nplayer]) + game.villager.game_key + game.minotaur.game_key
    GnossosBasicMovement2D.ttentry = tt_key

    game = GnossosBasicMovement2D(
        villager=villager,
        minotaur=minotaur,
        board_width=int(args.board_width),
        board_length=int(args.board_length),
        verbose=args.verbose)

    if args.do_pdb:
        pdb.set_trace()

    if args.solve:
        r, d, m = id_solve(game,
                           ai_depths=range(int(args.solver_depth_min), int(args.solver_depth_max)),
                           win_score=int(args.win_score),
                           tt=TT())
        print("result: {0}".format(r))
    else:
        game.play()


if __name__ == '__main__':  # pragma: no cover
    main(sys.argv)
